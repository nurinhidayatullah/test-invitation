# test-invitation

Test Invitation for Mile.app

1. Q : Do you prefer vuejs or reactjs? Why ?
   A : Having worked with both, i mush prever to use vue.js, the main reason being much clean template, much better state management,    fantastic documentation, and single file component out of the box.

2. Q : What complex things have you done in frontend development ?
A: I've been working on a progressive web app, made a carousel with vue and styling with pure CSS 

3. Q : why does a UI Developer need to know and understand UX? how far do you understand it?
A : Something that looks good but is hard to work with is a good example of UI, but bad in UX. Whereas something that very functional and useful but that looks bad is an example from a good UX but with a bad UI, so to make it balanced and shorter processing time between UI and UX is the reason why UI dev need to know and understand UX, in my opinion UI and UX are two important things that cannot be separated as i have explained above. I always try to make a simple interface, easy to use, and also looks good.

4. Q : Give your analysis results regarding https://taskdev.mile.app/login from the UI / UX side!
A : 1. The body display exceeding 100vh will be better if the min-height is set to 100vh.
    2. the navbar color does not match the basic color (in my opinion it is better if no background color and text color is changed to gray)
    3. If the screen size is reduced the contact person and the change language menu is missing,

5. Q : Create a better login page based on https://taskdev.mile.app/login and deploy on https://www.netlify.com (https://www.netlify.com/)!
A : link deploy = https://mile-app-login-clone.netlify.app

6. Q : Solve the logic problems below !
A : on gitlab repository at folder named '6'  
