// Number 6.a

let a = 3; b = 5;

[a, b] = [b, a]

console.log('=========Jawaban 6.a=======\n');

console.log(`nilai a = ${a}`);
console.log(`nilai b = ${b}\n`);

// Number 6.b

const numbers = [1, 2, 4, 5, 6, 7, 8, 10, 
                 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
                 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 
                 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
                 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 
                 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 
                 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 
                 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 
                 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 
                 94, 95, 96, 97, 98, 99, 100];

const bFunc = (arr) => {
    let i = 1; j = 0
    let missingNumbers = [];
    while(i <= 100) {
        if(arr[j] === i) {
            i++;
            j++;
        } else {
            missingNumbers.push(i);
            i++;
        }
    }
    return missingNumbers
}

console.log('=========Jawaban 6.b=======\n');

console.log(`Angka yang hilang adalah ${bFunc(numbers)} \n`)

// Number 6.c

const numbers2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
           11, 12, 13, 14, 15, 16, 17, 18, 
           19, 20, 21, 22, 23, 24, 25, 25, 
           25, 25, 25, 26, 27, 28, 29, 30, 
           31, 32, 33, 34, 34, 34, 34, 35, 
           36, 37, 38, 39, 40, 41, 42, 43, 
           44, 45, 46, 47, 48, 49, 50, 51, 
           52, 53, 54, 55, 56, 57, 58, 59, 
           60, 61, 62, 62, 63, 64, 65, 66, 
           67, 68, 69, 70, 71, 72, 73, 74, 
           75, 76, 77, 78, 79, 80, 81, 82, 
           83, 83, 83, 84, 85, 86, 87, 88, 
           89, 90, 91, 92, 92, 93, 94, 95, 
           96, 97, 98, 99, 100];

const cFunc = (arr) => {
    const filtered =  arr.filter((element, index, result) => result.indexOf(element) !== index);
    return [...new Set(filtered)];
}

console.log('=========Jawaban 6.c=======\n');

console.log(`Angka duplikat adalah ${cFunc(numbers2)} \n`)

// Number 6.d

const array_code = ["1.", "1.1.", "1.2.", "1.3.",
                    "1.4.", "1.1.1.", "1.1.2.", 
                    "1.1.3.", "1.2.1.", "1.2.2.", 
                    "1.3.1.", "1.3.2.", "1.3.3.", 
                    "1.3.4.", "1.4.1.", "1.4.3."];

const dFunc = (arr) => {
    let object_code = {}
    
    for(let i = 0; i < array_code.length; i++) {
      let splited = arr[i].split('.').filter(el => el != "");
      const [a, b, c] = [`${splited[0]}`, `${splited[1]}`, `${splited[2]}`];

      if(!object_code[a]) {
        object_code[a] = {}
      } else {
        if(!object_code[a][b]) {
            object_code[a][b] = {}
        } 
        if(splited.length > 2) {
            object_code[a][b][c] = arr[i]
        }
      }
    };

    return object_code
}

console.log('=========Jawaban 6.d=======\n');

console.log(`object_code = ${JSON.stringify(dFunc(array_code), 0, 4)} \n`)